import React,{ useReducer,useState } from 'react';
import logo from './logo.svg';
import './App.css';

const initialState = {
  imie: '',
  zawod: '',
  marzenie: '',
  audytor: '',
  stanGotowki: 0,

  nieruchomosciBiznesy: [],
  akcjeFundusze: [],

  przychodAktywny: 0,

  metaleSzlachetne: 0,
  oszczednosci: 0,

  kosztyStale: 0,
  kosztyZmienne: 0,
  kosztDziecka: 0,
  iloscDzieci: 0,

  pozyczkaKredytowa: 0,
};
const valState = {
  imie: 'Valeriya',
  zawod: 'QA managerka',
  marzenie: 'create,assist in creation, manage',
  audytor: 'Wojtek',
  stanGotowki: 2000,

  nieruchomosciBiznesy: [],
  akcjeFundusze: [],

  przychodAktywny: 8500,

  metaleSzlachetne: 0,
  oszczednosci: 2000,

  kosztyStale: 3400,
  kosztyZmienne: 2800,
  kosztDziecka: 500,
  iloscDzieci: 0,

  pozyczkaKredytowa: 0,
};
const wojState = {
  imie: 'Wojtek',
  zawod: 'Konsultant Tworzenia Produktów',
  marzenie: 'Budować / Tworzyć',
  audytor: 'Valeriya',
  stanGotowki: 2000,

  nieruchomosciBiznesy: [],
  akcjeFundusze: [],

  przychodAktywny: 10500,

  metaleSzlachetne: 0,
  oszczednosci: 2000,

  kosztyStale: 3000,
  kosztyZmienne: 5000,
  kosztDziecka: 500,
  iloscDzieci: 0,

  pozyczkaKredytowa: 0,
};

const actionNames = {
  WYBIERZ_GRACZA: 'WYBIERZ_GRACZA',
  DODAJ_BIZNES: 'DODAJ_BIZNES',
  DODAJ_AKCJE: 'DODAJ_AKCJE',
  DODAJ_METAL: 'DODAJ_METAL',
  DODAJ_KREDYT: 'DODAJ_KREDYT',
  DODAJ_DZIECKO: 'DODAJ_DZIECKO',
  DODAJ_ZWOLNIENIE: 'DODAJ_ZWOLNIENIE',
  DODAJ_WYDATEK: 'DODAJ_WYDATEK',
  DODAJ_PRZYCHOD: 'DODAJ_PRZYCHOD',
  DODAJ_WYPLATE: 'DODAJ_WYPLATE',

  SPRZEDAJ_BIZNES: 'SPRZEDAJ_BIZNES',
  SPRZEDAJ_AKCJE: 'SPRZEDAJ_AKCJE',
  SPRZEDAJ_METAL: 'SPRZEDAJ_METAL',
  SPLAC_KREDYT: 'SPLAC_KREDYT',

  COFNIJ_JEDEN_KROK: 'COFNIJ_JEDEN_KROK',
}

const isNotValid = action => action.payload && Object.values(action.payload).some(v => v === '')

function reducer(state,action) {
  switch(action.type) {

    case actionNames.WYBIERZ_GRACZA:
      return action.player === 'valeriya' ? valState : wojState;

    case actionNames.DODAJ_BIZNES:
      if(isNotValid(action)) return state;

      return {
        ...state,
        stanGotowki: state.stanGotowki - parseFloat(action.payload.wklad),
        nieruchomosciBiznesy: [...state.nieruchomosciBiznesy,action.payload],
        previousState: state
      };

    case actionNames.DODAJ_AKCJE:
      if(isNotValid(action)) return state;
      return {
        ...state,
        stanGotowki: state.stanGotowki - (parseFloat(action.payload.cena) * parseFloat(action.payload.ilosc)),
        akcjeFundusze: [...state.akcjeFundusze,action.payload],
        previousState: state
      };

    case actionNames.DODAJ_METAL:
      if(isNotValid(action)) return state;
      return {
        ...state,
        stanGotowki: state.stanGotowki - parseFloat(action.payload.koszt),
        metaleSzlachetne: state.metaleSzlachetne + parseFloat(action.payload.ilosc),
        previousState: state
      };

    case actionNames.DODAJ_KREDYT:
      if(isNotValid(action)) return state;
      return {
        ...state,
        stanGotowki: state.stanGotowki + parseFloat(action.payload),
        pozyczkaKredytowa: state.pozyczkaKredytowa + parseFloat(action.payload),
        previousState: state
      };

    case actionNames.DODAJ_DZIECKO:
      if(isNotValid(action)) return state;
      return {
        ...state,
        iloscDzieci: state.iloscDzieci + 1,
        previousState: state
      };

    case actionNames.DODAJ_ZWOLNIENIE:
      if(isNotValid(action)) return state;
      return {
        ...state,
        stanGotowki: state.stanGotowki - sumaKosztówSelector(state),
        previousState: state
      };

    case actionNames.DODAJ_WYDATEK:
      if(isNotValid(action)) return state;
      return {
        ...state,
        stanGotowki: state.stanGotowki - parseFloat(action.payload),
        previousState: state
      };

    case actionNames.DODAJ_PRZYCHOD:
      if(isNotValid(action)) return state;
      return {
        ...state,
        stanGotowki: state.stanGotowki + parseFloat(action.payload),
        previousState: state
      };

    case actionNames.DODAJ_WYPLATE:
      if(isNotValid(action)) return state;
      return {
        ...state,
        stanGotowki: state.stanGotowki + przeplywMiesiecznySelector(state),
        previousState: state
      };

    case actionNames.SPRZEDAJ_BIZNES:
      if(isNotValid(action)) return state;
      const isTheSameBiznes = (first,second) => (first.nazwa === second.nazwa && first.koszt === second.koszt && first.wklad === second.wklad && first.przeplyw === second.przeplyw)

      return {
        ...state,
        nieruchomosciBiznesy: state.nieruchomosciBiznesy.filter(biznes => !isTheSameBiznes(biznes,action.payload)),
        previousState: state
      };

    case actionNames.SPRZEDAJ_AKCJE:
      if(isNotValid(action)) return state;
      let iloscDoSprzedazy = parseFloat(action.payload.ilosc);
      const isTheSameAkcja = (first,second) => (first.kod === second.kod /*&& first.cena === second.cena && first.ilosc === second.ilosc*/)
      const newAkcjeFundusze = state.akcjeFundusze
        .map(akcja => {
          if(isTheSameAkcja(akcja,action.payload.akcja) && iloscDoSprzedazy > 0) {
            if(akcja.ilosc < iloscDoSprzedazy) {
              iloscDoSprzedazy = iloscDoSprzedazy - parseFloat(akcja.ilosc);
              return { ...akcja,ilosc: 0 }
            } else {
              const tmpIlosc = iloscDoSprzedazy
              iloscDoSprzedazy = 0;
              return { ...akcja,ilosc: parseFloat(akcja.ilosc) - parseFloat(tmpIlosc) };
            }

          }
          return akcja;
        })
        .filter(akcja => parseFloat(akcja.ilosc) !== 0)

      const przychodZeSprzedazy = parseFloat(action.payload.cena) * (parseFloat(action.payload.ilosc) - iloscDoSprzedazy);

      return {
        ...state,
        stanGotowki: state.stanGotowki + przychodZeSprzedazy,
        akcjeFundusze: newAkcjeFundusze,
        previousState: state
      };

    case actionNames.SPRZEDAJ_METAL:
      if(isNotValid(action)) return state;
      return {
        ...state,
        metaleSzlachetne: state.metaleSzlachetne - parseFloat(action.payload),
        previousState: state
      };

    case actionNames.SPLAC_KREDYT:
      if(isNotValid(action)) return state;
      return {
        ...state,
        stanGotowki: state.stanGotowki - parseFloat(action.payload),
        pozyczkaKredytowa: state.pozyczkaKredytowa - parseFloat(action.payload),
        previousState: state
      };

    case actionNames.COFNIJ_JEDEN_KROK:
      if(isNotValid(action)) return state;
      return state.previousState ? state.previousState : state;

    default:
      throw new Error();
  }
}

const przychodPasywnySelector = state => state.nieruchomosciBiznesy.reduce((suma,biznes) => suma + parseFloat(biznes.przeplyw),0);
const sumaPrzychodowSelector = state => state.przychodAktywny + przychodPasywnySelector(state);
const kosztDzieciSelector = state => state.kosztDziecka * state.iloscDzieci;
const splataPozyczkiSelector = state => state.pozyczkaKredytowa * 0.1;
const sumaKosztówSelector = state => state.kosztyStale + state.kosztyZmienne + kosztDzieciSelector(state) + splataPozyczkiSelector(state);
const przeplywMiesiecznySelector = state => sumaPrzychodowSelector(state) - sumaKosztówSelector(state);


const kredytHipotecznyBiznesu = biznes => biznes.koszt - biznes.wklad;


function App() {
  console.log('localStorage',JSON.parse(localStorage.getItem('gameState')))
  const [state,dispatch] = useReducer(reducer,JSON.parse(localStorage.getItem('gameState')) || initialState);
  const [nowyBiznes,setNowyBiznes] = useState({ nazwa: '',koszt: '',wklad: '',przeplyw: '' });
  const [noweAkcje,setNoweAkcje] = useState({ kod: '',cena: '',ilosc: '' });
  const [nowyMetal,setNowyMetal] = useState({ koszt: '',ilosc: '' });

  const [nowyKredyt,setNowyKredyt] = useState('');
  const [splataKredytu,setSplataKredytu] = useState('');

  const [nowyWydatek,setNowyWydatek] = useState('');
  const [nowyPrzychod,setNowyPrzychod] = useState('');

  const [sprzedajMetal,setSprzedajMetal] = useState('');
  const [sprzedajAkcje,setSprzedajAkcje] = useState({ ilosc: '',cena: '' });

  React.useEffect(() => {
    localStorage.setItem('gameState',JSON.stringify(state));
  },[state]);


  // console.log('state',state)
  if(state.imie === '') {
    return (
      <div className="App">
        <header className="App-header">
          <button onClick={() => dispatch({ type: actionNames.WYBIERZ_GRACZA,player: 'valeriya' })}>Wybierz Leruśkę</button>
          <button onClick={() => dispatch({ type: actionNames.WYBIERZ_GRACZA,player: 'wojtek' })}>Wybierz Wojtka</button>
        </header>
      </div>
    )
  }

  const resetFields = () => {
    setNowyBiznes({ nazwa: '',koszt: '',wklad: '',przeplyw: '' });
    setNoweAkcje({ kod: '',cena: '',ilosc: '' });
    setNowyMetal({ koszt: '',ilosc: '' });
    setNowyKredyt('');
    setSplataKredytu('');
    setNowyWydatek('');
    setNowyPrzychod('');
    setSprzedajMetal('');
    setSprzedajAkcje({ ilosc: '',cena: '' });
  }

  return (
    // <div className="App">
    <div class="grid-container">
      <div class="dodaj-aktywa">
        <div class="bzines-button"><button onClick={() => { dispatch({ type: actionNames.DODAJ_BIZNES,payload: nowyBiznes }); resetFields(); }}>Dodaj nieruchomość biznes</button></div>
        <div class="biznes-koszt">
          <input type="number" placeholder="Koszt" value={nowyBiznes.koszt} onChange={({ target }) => setNowyBiznes({ ...nowyBiznes,koszt: target.value })} />
          <input type="text" placeholder="Nazwa" value={nowyBiznes.nazwa} onChange={({ target }) => setNowyBiznes({ ...nowyBiznes,nazwa: target.value })} />
        </div>
        <div class="biznes-wklad"><input type="number" placeholder="Wkład" value={nowyBiznes.wklad} onChange={({ target }) => setNowyBiznes({ ...nowyBiznes,wklad: target.value })} /></div>
        <div class="biznes-przeplyw"><input type="number" placeholder="Przepływ" value={nowyBiznes.przeplyw} onChange={({ target }) => setNowyBiznes({ ...nowyBiznes,przeplyw: target.value })} /></div>
        <div class="dodaj-akcje"><button onClick={() => { dispatch({ type: actionNames.DODAJ_AKCJE,payload: noweAkcje }); resetFields(); }}>Dodaj akcje</button></div>
        <div class="akcje-kod"><input type="text" placeholder="Kod" value={noweAkcje.kod} onChange={({ target }) => setNoweAkcje({ ...noweAkcje,kod: target.value })} /></div>
        <div class="akcje-cena"><input type="number" placeholder="Cena" value={noweAkcje.cena} onChange={({ target }) => setNoweAkcje({ ...noweAkcje,cena: target.value })} /></div>
        <div class="akcje-ilosc"><input type="number" placeholder="Ilość" value={noweAkcje.ilosc} onChange={({ target }) => setNoweAkcje({ ...noweAkcje,ilosc: target.value })} /></div>
        <div class="dodaj-metale"><button onClick={() => { dispatch({ type: actionNames.DODAJ_METAL,payload: nowyMetal }); resetFields(); }}>Dodaj metal</button></div>
        <div class="metale-koszt"><input type="number" placeholder="Koszt" value={nowyMetal.koszt} onChange={({ target }) => setNowyMetal({ ...nowyMetal,koszt: target.value })} /></div>
        <div class="metale-ilosc"><input type="number" placeholder="Ilość" value={nowyMetal.ilosc} onChange={({ target }) => setNowyMetal({ ...nowyMetal,ilosc: target.value })} /></div>
      </div>
      <div class="dodaj-koszty">
        <div class="dodaj-kredyt"><button onClick={() => { dispatch({ type: actionNames.DODAJ_KREDYT,payload: nowyKredyt }); resetFields(); }}>Dodaj kredyt</button></div>
        <div class="kredyt-wartosc"><input type="number" placeholder="Koszt" value={nowyKredyt} onChange={({ target }) => setNowyKredyt(target.value)} /></div>
        <div class="splata-kredyt"><button onClick={() => { dispatch({ type: actionNames.SPLAC_KREDYT,payload: splataKredytu }); resetFields(); }}>Spłać kredyt</button></div>
        <div class="splata-wartosc"><input type="number" placeholder="Wartość" value={splataKredytu} onChange={({ target }) => setSplataKredytu(target.value)} /></div>
        <div class="dodaj-dziecko"><button onClick={() => { dispatch({ type: actionNames.DODAJ_DZIECKO }); resetFields(); }}>Dodaj dziecko</button></div>
        <div class="dodaj-zwolnienie"><button onClick={() => { dispatch({ type: actionNames.DODAJ_ZWOLNIENIE }); resetFields(); }}>Zwolnienie</button></div>
      </div>
      <div class="rejestr">
        <div class="wyplata"><button onClick={() => { dispatch({ type: actionNames.DODAJ_WYPLATE }); resetFields(); }}>Wyplata cashflow</button></div>
        <div class="dodaj-wydatki"><button onClick={() => { dispatch({ type: actionNames.DODAJ_WYDATEK,payload: nowyWydatek }); resetFields(); }}>Wydatek</button></div>
        <div class="wydatek-wartosc"><input type="number" placeholder="Koszt" value={nowyWydatek} onChange={({ target }) => setNowyWydatek(target.value)} /></div>
        <div class="dodaj-przychod"><button onClick={() => { dispatch({ type: actionNames.DODAJ_PRZYCHOD,payload: nowyPrzychod }); resetFields(); }}>Przychod</button></div>
        <div class="przychod-wartosc"><input type="number" placeholder="Koszt" value={nowyPrzychod} onChange={({ target }) => setNowyPrzychod(target.value)} /></div>
        <div class="stan-gotowki"><h4>Stan gotówki: </h4></div>
        <div class="stan-gotowki"><h2> {state.stanGotowki}</h2></div>
      </div>
      <div class="cashflow">
        <div class="przychod-pasywny"><h3>Przychód pasywny: <br />{przychodPasywnySelector(state)}</h3></div>
        <div class="przychod-aktywny"><h3>Przychód aktywny: <br />{state.przychodAktywny}</h3></div>
        <div class="cashflow-przychod"><h3>Suma przychodu: <br />{sumaPrzychodowSelector(state)}</h3></div>
        <div class="cashflow-koszty"><h3>Suma kosztów: <br />{sumaKosztówSelector(state)}</h3></div>
        <div class="przeplyw"><h3>Cashflow: <br />{przeplywMiesiecznySelector(state)}</h3></div>
      </div>
      <div class="przychod">
        <h2>Przychod</h2>
        <table>
          <tr>
            <td><b>Zarobki</b></td>
            <td>{state.przychodAktywny}</td>
          </tr>
          <tr>
            <th>Nieruchomości / Biznesy</th>
          </tr>
          {state.nieruchomosciBiznesy.map(biznes => (
            <tr>
              <td>{biznes.nazwa}</td>
              <td>{biznes.przeplyw}</td>
            </tr>
          ))}
        </table>
      </div>
      <div class="aktywa">
        <h2>Aktywa</h2>
        <table>
          {/* <tr>
            <td><b>Oszczędności</b></td>
            <td>{state.oszczednosci}</td>
          </tr> */}
          <tr>
            <td><b>Metale</b></td>
            <td>{state.metaleSzlachetne}</td>
            {state.metaleSzlachetne > 0 && <td><button onClick={() => dispatch({ type: actionNames.SPRZEDAJ_METAL,payload: sprzedajMetal })}>Sprzedaj</button></td>}
            {state.metaleSzlachetne > 0 && <td><input type="number" placeholder="Ilość" value={sprzedajMetal} onChange={({ target }) => setSprzedajMetal(target.value)} /></td>}
          </tr>
          <tr>
            <th>Akcje / Fundusze</th>
            <th>Ilość</th>
            <th>Cena</th>
          </tr>
          {state.akcjeFundusze.map(akcja => (
            <tr>
              <td>{akcja.kod}</td>
              <td>{akcja.ilosc}</td>
              <td>{akcja.cena}</td>
              <td><button onClick={() => dispatch({ type: actionNames.SPRZEDAJ_AKCJE,payload: { ...sprzedajAkcje,akcja } })}>Sprzedaj</button></td>
              <td><input type="number" placeholder="Ilość" value={sprzedajAkcje.ilosc} onChange={({ target }) => setSprzedajAkcje({ ...sprzedajAkcje,ilosc: target.value })} /></td>
              <td><input type="number" placeholder="Cena" value={sprzedajAkcje.cena} onChange={({ target }) => setSprzedajAkcje({ ...sprzedajAkcje,cena: target.value })} /></td>
            </tr>
          ))}
          <tr>
            <th>Nieruchomości / Biznesy</th>
            <th>Twój wkład</th>
            <th>Koszt</th>
          </tr>
          {state.nieruchomosciBiznesy.map(biznes => (
            <tr>
              <td>{biznes.nazwa}</td>
              <td>{biznes.wklad}</td>
              <td>{biznes.koszt}</td>
              <td><button onClick={() => dispatch({ type: actionNames.SPRZEDAJ_BIZNES,payload: biznes })}>Sprzedaj</button></td>
            </tr>
          ))}
        </table>
      </div>
      <div class="opis">
        <h3>Imię: {state.imie}</h3>
        <h3>Zawód: {state.zawod}</h3>
        <h3>Marzenie: {state.marzenie}</h3>
        <h3>Audytor: {state.audytor}</h3>
        <button onClick={() => dispatch({ type: actionNames.COFNIJ_JEDEN_KROK })}>Cofnij jeden krok</button>
      </div>
      <div class="koszty">
        <h2>Koszty</h2>
        <table>
          <tr>
            <td><b>Koszty stałe</b></td>
            <td>{state.kosztyStale}</td>
          </tr>
          <tr>
            <td><b>Koszty zmienne</b></td>
            <td>{state.kosztyZmienne}</td>
          </tr>
          <tr>
            <td><b>Rata kredytu</b></td>
            <td>{splataPozyczkiSelector(state)}</td>
          </tr>
          <tr>
            <td><b>Koszt utrzymania dziecka</b></td>
            <td>{state.kosztDziecka}x{state.iloscDzieci}={kosztDzieciSelector(state)}</td>
          </tr>
        </table>
      </div>
      <div class="pasywa">
        <h2>Pasywa</h2>
        <table>
          <tr>
            <td><b>Pożyczka bankowa</b></td>
            <td>{state.pozyczkaKredytowa}</td>
          </tr>
          <tr>
            <th>Nieruchomości / Biznesy</th>
            <th>Zobowiązania</th>
          </tr>
          {state.nieruchomosciBiznesy.map(biznes => (
            <tr>
              <td>{biznes.nazwa}</td>
              <td>{kredytHipotecznyBiznesu(biznes)}</td>
            </tr>
          ))}
        </table>
      </div>
    </div>

  );
}

export default App;
