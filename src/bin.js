"""

Write two functions,one that serializes a binary tree,given its root node and
one that deserializes the result of the first function from a string to a binary tree.



    1
    / \
2   3
    / / \
8  4   5


class Node
  Node left;
Node right;
int value;

"""
function serializeBinaryTree(node) {

    return JSON.stringify(serializeNode(node));
}


function serializeNode(node) {
    let result = {};

    //we want to go through each node 
    if(node.left) {
        result = { left: serializeNode(node.left) };
    }
    if(node.right) {
        result = { ...result,right: serializeNode(node.right) };
    }
    // from each node as a stop condition we want to return serialized node
    //   /return the result at the end 
    return { ...result,value: node.value };
}

function deserializeBinaryTree(serializedBinaryTree) {

    const binaryTree = JSON.parse(serializedBinaryTree);

    const result = {
        value: 1,
        left: {
            value: 2,
            left: {
                value: 8
            }
        },
        right: {
            value: 3,
            left: {
                value: 4
            },
            right: {
                value: 5
            }
        }
    }


    // start on the root

    // / for the leaf left you wannt to go inside annd the result put in the root.left

    // the same for the right 

    // as stop conndition if the is no more left or right than just return the created Node

    // at the end return whole tree


}
